location on anakin: ``/data/public/intelliseqngs/pgx/resources/intervals``

### Intervals for PGx with alts

**Last update date:** 7-12-2021

**How it was created (pgx-intervals_ALT-2mln-padded_custom-regions.interval_list):**
1. Copy genes from https://docs.google.com/spreadsheets/d/1KhSMf18fcbBrX1CO_8bREGTFUZTLVDbOzmAGLyxPbMU/edit?pli=1#gid=628114290
2. Get table from https://genome.ucsc.edu/cgi-bin/hgTables (paste gene list as identifiers, remaining options leave as default: hg38, Genes and Gene Predictions, GENCODE V38, knownGene, region: genome, output: all fields).
3. Save table and create initial bed: `tail -n +2 in.table | cut -d$'\t' -f 1-3 > in.bed`. 
4. Add in.bed into input json, set reference_genome to *hg38* and run ``cromwell run -i bed-to-interval-list.json https://gitlab.com/intelliseq/workflows/-/raw/bed-to-interval-list@1.2.1/src/main/wdl/tasks/bed-to-interval-list/bed-to-interval-list.wdl``

### Intervals for PGx without alts

**Last update date:** 13-12-2021

**How it was created (pgx-intervals_ALT-2mln-padded_custom-regions.interval_list):**
1. Copy pgx with alts bed (from subdirectory `bed`) above and remove alt chromosomes.
2. Add in.bed into input json, set reference_genome to *grch38-no-alt* and run ``cromwell run -i bed-to-interval-list.json https://gitlab.com/intelliseq/workflows/-/raw/bed-to-interval-list@1.2.1/src/main/wdl/tasks/bed-to-interval-list/bed-to-interval-list.wdl``


**History of updates - old versions:**

   04-02-2021 (dir: aldy_astrolabe_pharmcat_cyrius - intervals with genes for tools: astrolabe, pharmcat, aldy, cyrius)
   
   22-02-2012 (dir: pharmcat_astrolabe - intervals with genes for anly pharmcat and astrolabe - only for them gvcf is needed)


**How it was created - old versions (aldy_astrolabe_pharmcat_cyrius/pgx-intervals-2mln-padded_custom-regions.interval_list):**
1. File `aldy_astrolabe_pharmcat_cyrius/pgx-intervals.bed`: save tsv with columns chr, start, stop as .bed - from https://docs.google.com/spreadsheets/d/1KhSMf18fcbBrX1CO_8bREGTFUZTLVDbOzmAGLyxPbMU/edit?usp=sharing
2. Run``cromwell run -i bed-to-interval-list.json https://gitlab.com/intelliseq/workflows/-/raw/bed-to-interval-list@1.2.1/src/main/wdl/tasks/bed-to-interval-list/bed-to-interval-list.wdl``
