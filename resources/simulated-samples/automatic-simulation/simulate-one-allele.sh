GENE=$1
ALLELE=$2
OUTPUT_DIR=$3
if [ -z "$4" ]; then 
 PADDING=500
else
 PADDING=$4
fi

mkdir run-$GENE-$ALLELE-$PADDING
cd run-$GENE-$ALLELE-$PADDING
mkdir inputs outputs

#create inputs: bed and vcf
bcftools view -Oz -s $ALLELE ../vcfs-all-genes-all-alleles/$GENE.vcf.gz > inputs/in.vcf.gz
tabix -p vcf inputs/in.vcf.gz
cat ../beds/NUDT15.bed | awk -F'\t' -v p="$PADDING" '{print $1"\t"$2-p"\t"$3+p}' > inputs/in.bed

#prepare jsons for cromwell
cp ../templates/inputs.json .
cp ../templates/options.json .

SAMPLE_NAME=$GENE-$ALLELE-p$PADDING
sed -i "s#\$SAMPLE_NAME#$SAMPLE_NAME#" inputs.json

#run simulation
cromwellnodb run -i inputs.json -o options.json https://gitlab.com/intelliseq/workflows/raw/dev/src/main/wdl/tasks/simulate-neat/simulate-neat.wdl > log 2>&1

#copy fastqs and remove everything else
cp outputs/*.fq.gz ../$OUTPUT_DIR
cd ..
rm -rf run-$GENE-$ALLELE-$PADDING
