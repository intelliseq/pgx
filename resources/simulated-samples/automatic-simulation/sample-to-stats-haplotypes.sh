mkdir -p tmp-run-sim-to-stats simulated_samples stats

for i in {1..100}; do

 echo $i
 SAMPLE_NAME="simulated_"$i
 bash simulate-full-sample.sh $SAMPLE_NAME simulated_samples


 cd tmp-run-sim-to-stats

 cp ../templates/inputs-pgx.json .
 cp ../templates/options.json .
 sed -i "s#\$SAMPLE_NAME#$SAMPLE_NAME#" inputs-pgx.json
 cromwellnodb run -i inputs-pgx.json -o options.json \
	 https://gitlab.com/intelliseq/pgx/-/raw/delete-pgx-genotyping-report-module/src/main/wdl/pipelines/pgx.wdl > log 2>&1


 cd ..

 python3 polygenic-stats.py \
	--polygenic tmp-run-sim-to-stats/outputs/$SAMPLE_NAME-polygenic-haplotype.json \
	--original-alleles simulated_samples/$SAMPLE_NAME-alleles.csv \
	--output stats/$SAMPLE_NAME

 rm -rf tmp-run-sim-to-stats >> log 2>&1

done
