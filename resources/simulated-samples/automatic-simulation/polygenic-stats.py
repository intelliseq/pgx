#!/usr/bin/python3

import argparse
import json
import pandas
import csv

__version__ = '1.0.0'


def load_json(json_path: str) -> dict:
    with open(json_path, "r") as file:
        return json.load(file)


def load_csv(csv_path: str):
    with open(csv_path, "r") as csv_file:
        return pandas.read_csv(csv_file, names=["gene", "allele"], dtype={'allele': 'str'})


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Concat jsons')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--polygenic', type=str, required=True, help='JSON with polygenic haplotypes. Output from PGX workflow (ID-polygenic-haplotype.json).')
    parser.add_argument('--original-alleles', type=str, required=True, help='Original alleles from simulation (${SAMPLE_NAME}-alleles.csv).')
    parser.add_argument('--output', type=str, required=True, help='Output filename or path.')
    args = parser.parse_args()

    polygenic = load_json(args.polygenic)
    original_alleles = load_csv(args.original_alleles)
    wrongs = []
    results_phased, results_unphased = [], []

    for i in range(19):
        gene = polygenic['polygenic'][i]["gene"]
        original_allele = original_alleles.loc[original_alleles["gene"] == gene]["allele"].values[0]
        sample_name = args.original_alleles.replace("-alleles.csv", "")
        phased, unphased = [], []

        for haplotype in polygenic['polygenic'][i]['haplotypes']['phased']:
            phased += [h.replace(gene + "*", "") for h in haplotype.split("_")]
        for haplotype in polygenic['polygenic'][i]['haplotypes']['unphased']:
            unphased += [h.replace(gene + "*", "") for h in haplotype.split("_")]

        if original_allele in phased:
            results_phased += [[gene, 1]]
        elif gene == "CYP2R1":
            results_phased += [[gene, "NA"]]
        else:
            results_phased += [[gene, 0]]
            wrongs += [[sample_name, gene, "phased"]]

        if original_allele in unphased:
            results_unphased += [[gene, 1]]
        else:
            results_unphased += [[gene, 0]]
            wrongs += [[sample_name, gene, "unphased"]]

    with open(args.output+"-phased.csv", 'w', newline='') as file:
        writer = csv.writer(file)
        for row in results_phased:
            writer.writerow(row)

    with open(args.output+"-unphased.csv", 'w', newline='') as file:
        writer = csv.writer(file)
        for row in results_unphased:
            writer.writerow(row)

    with open(args.output+"-wrong-alleles.csv", 'w', newline='') as file:
        writer = csv.writer(file)
        for row in wrongs:
            writer.writerow(row)
