#!/bin/bash

PHARMVAR_VERSION=5.2.14
DIR="pharmvar-"$PHARMVAR_VERSION

echo "PART 1: PROCESS VCFS FOR ALL ALLELES; ADD GT 1/1; REMOVE DUPLICATES"

for GENE in CYP26A1 CYP2B6 CYP2C8 CYP2D6 CYP2J2 CYP2S1 CYP3A4 CYP3A5 CYP4F2 SLCO1B1 CYP2A13 CYP2C19 CYP2C9 CYP2F1 CYP2R1 CYP2W1 CYP3A43 CYP3A7 NUDT15; do

 echo "NOW PROCESSING: "$GENE
 OUT="out/"$GENE
 mkdir -p $OUT

 for file in $DIR/$GENE/GRCh38/*; do
  NAME=$( basename $file )
  i=$( basename $file | sed "s/$GENE\_//" | sed 's/.vcf//' )
  grep ^## $file > $OUT/$NAME
  echo -e "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t"$i >> $OUT/$NAME
  grep -v ^# $file | sed 's/$/\tGT\t1\/1/g'>> $OUT/$NAME
 done

 echo "REMOVING DUPLICATES... REMOVED FILES:"
 #create list of alleles which have main allele (*number*) because
 #sometimes suballeles (eg *number*.001) are identical to main allele;
 #check and remove if duplicated
 ls $OUT| sed "s/$GENE\_//" | sed "s/\.vcf//" | grep -v "\." | sort -n > LIST

 while read i; do
  if [ -f $OUT/${GENE}_$i.vcf ]; then
   grep -v ^# $OUT/${GENE}_$i.vcf > allele
   for file in $OUT/${GENE}_$i.0*; do
    grep -v ^# $file > suballele
    cmp -s allele suballele && (basename $file; rm $file) 
   done
  fi
 done < LIST
 rm -f allele suballele LIST

done

echo "DONE PART 1"
echo "PART 2: MERGE ALLELES FOR EACH GENE"
mkdir -p vcfs-all-genes-all-alleles

for GENE in CYP26A1 CYP2B6 CYP2C8 CYP2D6 CYP2J2 CYP2S1 CYP3A4 CYP3A5 CYP4F2 SLCO1B1 CYP2A13 CYP2C19 CYP2C9 CYP2F1 CYP2R1 CYP2W1 CYP3A43 CYP3A7 NUDT15; do
 echo "NOW PROCESSING: "$GENE
 for file in out/$GENE/*; do
  bgzip $file
  tabix -p vcf $file.gz
 done
 ls out/$GENE/*| grep -v tbi | sort -V > alleles_merge.list
 bcftools merge -0 -Oz -l alleles_merge.list > vcfs-all-genes-all-alleles/$GENE.vcf.gz
 tabix -p vcf vcfs-all-genes-all-alleles/$GENE.vcf.gz
 rm out/$GENE/*
done
rm alleles_merge.list
rm -r out

echo "DONE PART 2"
echo "FINISHED: VCFS IN DIRECTORY 'vcfs-all-genes-all-alleles'"