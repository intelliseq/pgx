### Creation of VCF files with all alleles for all genes

#### Current version:

Date: 20.12.2022

PharmVar: 5.2.14 - last updated 02.12.2022


#### Instructions:

0. Create a separate directory and move into it.
1. Download database from https://www.pharmvar.org/download (click Download Complete Database) and unzip.
2. Remove folders `GRCh37` and `RefSeqGene` and file `*.haplotypes.fasta` for each gene. Remove `GRCh38/*.haplotypes.tsv` and directory `CYP2D6/M33388`. Also gene `DPYD` should be removed - it does not have star alleles, only rs. 
```
PHARMVAR_VERSION=5.2.14
unzip pharmvar-$PHARMVAR_VERSION.zip
cd pharmvar-$PHARMVAR_VERSION
rm -r */GRCh37 */RefSeqGene CYP2D6/M33388
rm */*.haplotypes.fasta */GRCh38/*.haplotypes.tsv
rm -r DPYD
cd ..
```
3. Copy `create-vcfs-all-genes-all-alleles.sh` to your directory and check/update pharmvar version (`PHARMVAR_VERSION` variable).
4. Run `create-vcfs-all-genes-all-alleles.sh`.
5. Copy `vcfs-all-genes-all-alleles` content to `/data/public/intelliseqngs/pgx/resources/simulated-samples/automatic-simulation/vcfs-all-genes-all-alleles` and remove your directory.
6. Update current version section in this readme.


### Bed files
Bed files were created according to https://www.ncbi.nlm.nih.gov/gene GRCh38 location.


### Scripts

Script `run-simulation.sh` runs `simulate-one-allele.sh` for each gene and allele. `simulate-one-allele.sh` is run: 
```bash
bash simulate-one-allele.sh $GENE $ALLELE $OUTPUT_DIRECTORY $PADDING
```
where `$PADDING` is optional with default 500 (should be a non-negative integer, it is how much base pairs to add to simulated interval on both sides, the interval is taken from corresponding bed file). Produced fastq samples are named `$GENE-$ALLELE-p$PADDING_1.fq.gz` and `$GENE-$ALLELE-p$PADDING_2.fq.gz`.


### Creation of samples

Assuming that simulated alleles created in previous step are in directory `samples-one-allele`, to create 100 samples just run script `sample-to-stats.sh`. It consists of three main steps:
1. Running `simulate-full-sample.sh` - where sample is created by randomly selecting alleles and combining them.
2. Running PGx pipeline on this sample. 
3. Running `polygenic-stats.py`, which returns if polygenic returned good haplotype according to selected alleles in step 1.


### Creation of report

To create table summarizing, how many alleles were accurate for each gene, run `final-stats.R`. It will create `final-stats-batch1.csv` file.
