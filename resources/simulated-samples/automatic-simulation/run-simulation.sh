for GENE in CYP26A1 CYP2B6 CYP2C8 CYP2D6 CYP2J2 CYP2S1 CYP3A4 CYP3A5 CYP4F2 SLCO1B1 CYP2A13 CYP2C19 CYP2C9 CYP2F1 CYP2R1 CYP2W1 CYP3A43 CYP3A7 NUDT15; do
 echo $GENE
 while read ALLELE; do
  echo $ALLELE
  bash simulate-one-allele.sh $GENE $ALLELE samples-one-allele
 done < vcfs-all-genes-all-alleles/$GENE-alleles.list
done
