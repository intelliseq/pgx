SAMPLE_NAME=$1
OUTPUT_DIR=$2
if [ -z "$3" ]; then 
 PADDING=500
else
 PADDING=$3
fi

mkdir -p $OUTPUT_DIR
touch $OUTPUT_DIR/${SAMPLE_NAME}_1.fq.gz $OUTPUT_DIR/${SAMPLE_NAME}_2.fq.gz $OUTPUT_DIR/${SAMPLE_NAME}-alleles.csv

for GENE in CYP26A1 CYP2B6 CYP2C8 CYP2D6 CYP2J2 CYP2S1 CYP3A4 CYP3A5 CYP4F2 SLCO1B1 CYP2A13 CYP2C19 CYP2C9 CYP2F1 CYP2R1 CYP2W1 CYP3A43 CYP3A7 NUDT15; do
 ALLELE=$(shuf -n 1 vcfs-all-genes-all-alleles/$GENE-alleles.list)
 cat samples-one-allele/$GENE-$ALLELE-p${PADDING}_1.fq.gz >> $OUTPUT_DIR/${SAMPLE_NAME}_1.fq.gz
 cat samples-one-allele/$GENE-$ALLELE-p${PADDING}_2.fq.gz >> $OUTPUT_DIR/${SAMPLE_NAME}_2.fq.gz
 echo -e $GENE","$ALLELE >> $OUTPUT_DIR/${SAMPLE_NAME}-alleles.csv
done
