# Pgx test samples

**Samples directory on anakin:** `data/public/intelliseqngs/pgx/resources/simulated-samples`
(subdirectories: `cyps-star-1-a`, `diplotypes`, `haplotypes`, `other`)


This directory contains synthetic samples created to validate and test pharmacogenomics software. Created using multistep process, described in **Steps to create pgx sample**

## Contents
- [Pgx test samples](#pgx-test-samples)
  * [Samples](#samples)
    + [Haploids](#haploids)
    + [Diploids](#diploids)
  * [Steps to create pgx sample](#steps-to-create-pgx-sample-with-cyp2d6)
- [Samples with one rs for all CYP2D6 rsids](#samples-with-one-rs-for-all-cyp2d6-rsids)



## Samples

There are haploids and diploid combinations, small fragments and whole chromosomes.
Used gene is CYP2D6 and two paralogs: CYP2D7 and CYP2D6P.
Variants were choosen arbitrally from PharmVar database: https://www.pharmvar.org/gene/CYP2D6

Each sample has up to 3 versions. All versions for one sample have the same set of variants (star allele combination) but differ in simulated region (it can be: region flanked with 22 mln bp, whole chromosome or all chromosomes). Simulated region is defined in bed file.
- 22 mln - CYP2D6, CYP2D7 and CYP2D6P region flanked with 22 mln basepairs. Bed: https://gitlab.com/intelliseq/pgx/-/blob/dev/resources/bed/cyps_2mln.bed
- chrom - whole chromosome 22 with variants included the same as in 22 mln. Bed: https://gitlab.com/intelliseq/pgx/-/blob/dev/resources/bed/chr22.bed
Most of the samples have only 22 mln flanked regions, as it is sufficient for testing purposes.


Diploids are created by combining fastq files from two haploids. Example:
```
zcat h1_1.fq.gz > 1.fq
zcat h1_2.fq.gz > 2.fq
zcat h2_1.fq.gz >> 1.fq
zcat h2_2.fq.gz >> 2.fq
bgzip 1.fq
bgzip 2.fq
```


### Haploids:

| Variant | sample-name | Created versions | Coverage | Date, author | Comment |
|-|-|-|-|-|-|
| CYP2D6\*1 | cyps-1-a | 22 mln + chrom22 | 30 and 15| moni.krzyz, 3.11.2020 | Version "a" |
| CYP2D6\*1 | cyps-1-b | 22 mln + chrom22 | 30 and 15  | moni.krzyz, 4.11.2020 | Version "b" |
| CYP2D6\*8 | cyps-8 | 22 mln | 30 and 15 | moni.krzyz, 4.11.2020 |  |
| CYP2D6\*14 | cyps-14 | 22 mln | 30 and 15 | moni.krzyz, 6.11.2020 ||
| CYP2D6\*28 | cyps-28 | 22 mln | 30 and 15| moni.krzyz, 6.11.2020 ||
| CYP2D6\*35 | cyps-35 | 22 mln | 30 and 15 | moni.krzyz, 5.11.2020 | |
| CYP2D6\*36 | cyps-36 | 22 mln | 30 and 15 | moni.krzyz, 6.11.2020 | |
| CYP2D6\*40 | cyps-40 | 22 mln | 30 and 15 | moni.krzyz, 6.11.2020 ||
| CYP2D6\*59 | cyps-59 | 22 mln | 30 and 15 | moni.krzyz, 6.11.2020 ||
| CYP2D6\*82 | cyps-82 | 22 mln | 30 and 15 | moni.krzyz, 6.11.2020 | |
| CYP2D6\*83 | cyps-83 | 22 mln | 30 and 15 | moni.krzyz, 6.11.2020 | |
| CYP2D6\*101 | cyps-101 | 22 mln | 30 and 15 | moni.krzyz, 5.11.2020 ||
| CYP2D6\*5 | cyps-5 | 22 mln | 15 | moni.krzyz, 6.04.2021 | large deletion 9,001,750 removes paralog gene,i nsv3900214, regions defined in /cyp2d6_del_and_paralogs.bed (deletion flanked with 22 mln nucleotides) |


### Diploids

(*coverage 60 made from haploids 30 + 30 and coverage 30 made from haploids 15 + 15 )

| Variant | sample-name | Created versions | Date, author | Comment | coverage|
|-|-|-|-|-|-| 
| CYP2D6\*1/CYP2D6\*1| cyps-1a-1b| 22 mln + chrom22| moni.krzyz, 5.11.2020 | | 30 and 60 |
| CYP2D6\*1/CYP2D6\*8 | cyps-1a-8 | 22 mln | moni.krzyz, 5.11.2020 | | 30 and 60  |
| CYP2D6\*14/CYP2D6\*28 | cyps-14-28 | 22 mln | moni.krzyz, 6.11.2020 |  |30 and 60   |
| CYP2D6\*35/CYP2D6\*36 | cyps-35-36 | 22 mln | moni.krzyz, 6.11.2020 |  | 30 and 60  |
| CYP2D6\*40/CYP2D6\*59 | cyps-40-59 | 22 mln | moni.krzyz, 6.11.2020 |   |30 and 60  |
| CYP2D6\*82/CYP2D6\*101 | cyps-82-101 | 22 mln | moni.krzyz, 6.11.2020 |  | 30 and 60  |
| CYP2D6\*1-a/CYP2D6\*5 | cyps-1a-5 | 22 mln | moni.krzyz, 6.11.2020 | different regions for each haplotype - problem with too low coverage | | 30  |


### Other
| Variant | sample-name | Created versions | Date, author | Comment | coverage|
|-|-|-|-|-|-| 
| None| cyrius-regions |500 and 22000  bases flanked | moni.krzyz, 31.12.2020 | | 30 |

Cyrius regions - to test if cyrius software returns proper results if all needed regions are included

```
wget https://raw.githubusercontent.com/Illumina/Cyrius/master/data/CYP2D6_region_38.bed
cat CYP2D6_region_38.bed | awk 'BEGIN{OFS="\t"}{print $1, $2 - 500, $3 + 500}' > flanked.bed
sortBed -chrThenSizeD -i flanked.bed > sorted.bed
```
Then use sorted.bed to simulate-neat.wdl (all chromosomes, ploidy=2, coverage=30)

## Steps to create pgx sample with CYP2D6
**Note**: No structural variants. Script was tested only for CYP2D6 gene. Every time you use check CYP2D6-star-*.vcf.gz and view golden bam in IGV browser.

STEPS 1 and 2 are already done
______________________
1. Create bed file to take random mutations from gnomad for regions outside of the pharmacogene
2. Save variants from this region to vcf file:

```
bcftools view -R ${PROJECT_DIR}/resources/bed/cyp2d7_2d8p_2mln.bed /data/public/intelliseqngs/workflows/resources/snps-and-indels-annotations/frequencies/gnomad-genomes-v3/04-11-2019/chr22.gnomad-genomes-v3.vcf.gz | bgzip > /tmp/temporary-resources/cyps-gnomad.vcf.gz

```
-----------------------------
3. Run Script:
```
STAR="1"
GENE="CYP2D6"
PGX_VCF_GZ="/data/public/intelliseqngs/workflows/resources/pgx/pharmcat/14-06-20/pgx.vcf.gz"

bash ${PROJECT_DIR}/resources/simulated-samples/create-inputs/create-inputs.sh $PGX_VCF_GZ $STAR $GENE

```
4. Run simulate-neat.wdl with input created by script runned above. All data needed to find outputs is printed in the console.


# Samples with one rs for all CYP2D6 rsids

**Samples directory on anakin:** `/data/public/intelliseqngs/pgx/resources/simulated-samples/for-each-rs`

Directory content:
```bash
/data/public/intelliseqngs/pgx/resources/simulated-samples/for-each-rs
├── cyp2d6.rsid.vcf.gz #all rsids in CYP2D6 region
├── cyp2d6.rsid.vcf.gz.tbi
├── genotyping #pgx outputs and additional vcfs: filtered and rs*-final from gvcf
├── inputs #for simulating and genotyping
├── options #for simulating and genotyping
├── simulated #results of simulation: fastq, golden bam and vcf
└── vcf #one vcf with one variant for each rs, used to simulate samples
```
   

Created by script: https://gitlab.com/intelliseq/pgx/-/blob/dev/resources/simulated-samples/simulate-rsid.sh



