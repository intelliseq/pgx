
### Current version: 

**Date**: 10.06.2022

**PharmVar:** 5.1.15 - last updated 31.05.2022


### Instructions:

1. Download data from https://www.pharmvar.org/gene/CYP2D6 (click Download Gene Data).
2. Run:
```bash
VERSION=5.1.15
unzip CYP2D6-$VERSION.zip
cd CYP2D6-$VERSION
rm GRCh38/CYP2D6.NC_000022.11.haplotypes.tsv
for file in GRCh38/*; do
  bgzip $file
  tabix -p vcf $file.gz
done
ls GRCh38/* | grep -v tbi > list
bcftools concat -a -D -f list | bcftools sort -Oz -o CYP2D6-all-pharmvar-positions.vcf.gz
tabix -p vcf CYP2D6-all-pharmvar-positions.vcf.gz
```
3. Copy `CYP2D6-all-pharmvar-positions.vcf.gz` with index to `/data/public/intelliseqngs/pgx/resources/CYP2D6-all-pharmvar-positions/` and remove your directory.
4. Update current version section in this readme.
