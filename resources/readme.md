#  RESOURCES

The location of resources:
  - **anakin** (main location):
    - directory: **/data/public/intelliseqngs/pgx/resources**
  - **kenobi** (rsynced to main location on anakin):
    - directory: **/data/public/intelliseqngs/pgx/resources**

---

## Table of Contents

**Resources**
  * [***BREF3 reference***](./bref3-reference/readme.md)
  * [***Interval files***](./intervals/readme.md)
  * [***VCF to genotype by***](./vcf-to-genotype-by/readme.md)
  * [***Pharmacogenes SNP (no haplotypes) vcf***](./pgx-only-snp/readme.md)

## List of genes

* CYP26A1
* CYP2A13
* CYP2B6
* CYP2C19
* CYP2C8
* CYP2C9
* CYP2D6
* CYP2F1
* CYP2J2
* CYP2R1
* CYP2S1
* CYP2W1
* CYP3A4
* CYP3A43
* CYP3A5
* CYP3A7
* CYP4F2
* NUDT15
* SLCO1B1

## Comments

* CYP2R1 can't be phased by beagle, because it has only one star allele with only one rs.
 
