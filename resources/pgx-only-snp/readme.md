**Directory contents (anakin):**

```bash
/data/public/intelliseqngs/pgx/resources/pgx-only-snp/
├── pgx-snp.vcf.gz
└── pgx-snp.vcf.gz.tbi
```

**How it was created (19.08.2022)**

For genes ABCG2, CACNA1S, CFTR, RYR1, VKORC1 data was created from _Allele Definition Table_ for each gene from [PharmCKB-CPIC resources](https://www.pharmgkb.org/page/pgxGeneRef).

For genes BCHE and F5 data was taken from PharmGKB database (all rsIDs from [BCHE table](https://www.pharmgkb.org/gene/PA25294/variantAnnotation) and [F5 table](https://www.pharmgkb.org/gene/PA159/variantAnnotation)).


