#!/bin/bash
VERSION="0.0.1"
TARGET1="/data/public/intelliseqngs/pgx/resources/simulated-sv-samples/fasta-files/pharmacogenes-fasta"
TARGET2="/data/public/intelliseqngs/pgx/resources/simulated-sv-samples/fasta-files/cyrius-panel-fasta"
wget -O pharmacogenes.fa 'http://www.ensembl.org/biomart/martservice?query=<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE Query><Query virtualSchemaName = "default" formatter = "FASTA" header = "0" uniqueRows = "0" count = "" datasetConfigVersion = "0.6" ><Dataset name = "hsapiens_gene_ensembl" interface = "default" ><Filter name = "upstream_flank" value = "1000"/><Filter name = "downstream_flank" value = "1000"/><Filter name = "chromosome_name" value = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,MT,X,Y"/><Filter name = "external_gene_name" value = "CYP26A1,CYP2A13,CYP2B6,CYP2C19,CYP2C8,CYP2C9,CYP2F1,CYP2J2,CYP2R1,CYP2S1,CYP2W1,CYP3A4,CYP3A43,CYP3A5,CYP3A7,CYP4F2,NUDT15,SLCO1B1"/><Attribute name = "gene_exon_intron" /><Attribute name = "external_gene_name" /></Dataset></Query>'
samtools faidx pharmacogenes.fa 

wget -O "$TARGET1"/pharmacogenes.tsv 'http://www.ensembl.org/biomart/martservice?query=<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE Query><Query  virtualSchemaName = "default" formatter = "TSV" header = "0" uniqueRows = "0" count = "" datasetConfigVersion = "0.6" ><Dataset name = "hsapiens_gene_ensembl" interface = "default" ><Filter name = "chromosome_name" value = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,MT,X,Y"/><Filter name = "external_gene_name" value = "CYP26A1,CYP2A13,CYP2B6,CYP2C19,CYP2C8,CYP2C9,CYP2F1,CYP2J2,CYP2R1,CYP2S1,CYP2W1,CYP3A4,CYP3A43,CYP3A5,CYP3A7,CYP4F2,NUDT15,SLCO1B1"/><Attribute name = "chromosome_name" /><Attribute name = "start_position" /><Attribute name = "end_position" /><Attribute name = "external_gene_name" /></Dataset></Query>'
awk 'BEGIN {OFS="\t"} {print "chr"$1,$2-1001,$3+1000,$4}' "$TARGET1"/pharmacogenes.tsv | sort -k1,1V -k2,2n -k3,3n > "$TARGET1"/pharmacogenes-padded.bed
printf "chr22\t42125498\t42150455\tCYP2D6;CYP2D7\n" >> "$TARGET1"/pharmacogenes-padded.bed

wget -O "$TARGET2"/CYP2D6_region_38.bed https://raw.githubusercontent.com/Illumina/Cyrius/master/data/CYP2D6_region_38.bed
grep 'norm' "$TARGET2"/CYP2D6_region_38.bed | cut -f1-3 | sort -k1,1V -k2,2n -k3,3n | sed 's/\t/:/' | sed 's/\t/-/'  > "$TARGET2"/cyrius-norm-regions.tsv
samtools faidx /data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa \
-r "$TARGET2"/cyrius-norm-regions.tsv -o "$TARGET2"/cyrius-norm-regions.fa
samtools faidx "$TARGET2"/cyrius-norm-regions.fa
