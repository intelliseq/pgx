#!/bin/bash

VERSION="0.0.1"

set -e -o pipefail

TARGET_DIR="/data/public/intelliseqngs/pgx/resources/simulated-sv-samples/fasta-files/long-ref-fasta"

## duplication between REP6 and REP7 region (22:42123846-42135998)
chrom_length=$(grep -m1 "SN:chr22" /data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.dict | grep -o 'LN:[0-9]*' | cut -f2 -d ':')
## define regions before, inside and following the duplication
R1=chr22:1-42135998 #from beginning of chrom22 to breakpoint within REP7
R2=chr22:42123846-42135998 ## REP6-REP7
R3=chr22:42135998-"$chrom_length"
## extract the above regions
samtools faidx /data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa "$R1" -o tmp1.fa
samtools faidx /data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa "$R2" -o tmp2.fa
samtools faidx /data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa "$R3" -o tmp3.fa
## create one fasta (all seq in one line)
printf ">chr22\n" | cat - <(cat tmp1.fa tmp2.fa tmp3.fa | grep -v '>' | tr '\n' '#' | sed 's/#$/\n/' | sed 's/#//g' ) > tmp.fa
samtools faidx tmp.fa
## reformat fasta
samtools faidx tmp.fa chr22 -o "$TARGET_DIR"/between_rep_cyp2d6_dup.fa
samtools faidx "$TARGET_DIR"/between_rep_cyp2d6_dup.fa
rm tmp*

