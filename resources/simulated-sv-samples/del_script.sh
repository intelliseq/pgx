#!/bin/bash

VERSION="0.0.1"

set -e -o pipefail

TARGET_DIR="/data/public/intelliseqngs/pgx/resources/simulated-sv-samples/fasta-files/long-ref-fasta"
while read f
do
  if ! echo $f | grep -q '^#';then
    name=$(echo $f | cut -f1 -d ' ' )
    start=$(echo $f | cut -f3 -d ' ')
    break1=$(( $start-1 )) 
    end=$(echo $f | cut -f4 -d ' ')
    break2=$(( $end +1 ))
    chrom=$(echo $f | cut -f2 -d ' ')
    chrom_length=$(grep -m1 "SN:$chrom" /data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.dict | grep -o 'LN:[0-9]*' | cut -f2 -d ':')
    ## define no deletion regions
    R1="$chrom":1-"$break1"
    R2="$chrom":"$break2"-"$chrom_length"
    ## extract no deletion regions
    samtools faidx /data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa "$R1" -o tmp1.fa
    samtools faidx /data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa "$R2" -o tmp2.fa
    ## create one fasta (all seq in one line)
    printf ">$chrom\n" | cat - <(cat tmp1.fa tmp2.fa | grep -v '>' | tr '\n' '#' | sed 's/#$/\n/' | sed 's/#//g' ) > tmp.fa
    samtools faidx tmp.fa
    ## reformat fasta
    samtools faidx tmp.fa "$chrom" -o "$TARGET_DIR"/"$name".fa
    samtools faidx "$TARGET_DIR"/"$name".fa
    rm tmp* 
  fi
done<cyp2d6_del.tsv
