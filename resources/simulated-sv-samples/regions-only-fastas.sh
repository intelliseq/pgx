#!/bin/bash
VERSION="0.0.1"

for i in long-ref-fasta/*.fa
do
  name=$(basename $i | sed 's/.fa//')
  grep -m"$1" "$name" cyp2d6-regions.tsv | tail -1 | cut -f2- >  "$name.bed"
  region=$(cat $name.bed | sed 's/\t/:/' | sed 's/\t/-/')
  samtools faidx $i $region -o $name.fa
  sed -i 's/>.*$/>chr22/' $name.fa
  samtools faidx $name.fa
done
