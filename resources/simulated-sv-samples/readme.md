# Simulated SV samples 
  
**Samples directory on anakin:** `/data/public/intelliseqngs/pgx/resources/simulated-sv-samples`
(subdirectories: `reference-reads`, `diploid-reads`, `haploid-reads`, `fasta-files`)  

## Contents
* [Samples description](#samples-description)
    + [Variants](#variants)
    + [Reference reads](#reference-reads)  
      * wgs-1kb-30x-pharmacogenes  
      * wgs-1kb-30x-cyrius-panel
    + [Haploid reads](#haploid-reads)  
      * wgs-100kb-15x-cyp2d6
      * wgs-1kb-15x-cyp2d6
    + [Diploid reads](#diploid-reads)  
      * ready-diploid-wgs-100kb-30x-cyp2d6 (for smoove/lumpy)
      * ready-diploid-wgs-1kb-30x-pharmacogenes (for ExomeDepth, but ref counts needed) 
      * ready-diploid-wgs-1kb-30x-cyrius-panel (for Cyrius or ExomeDepth, but for the latter ref-counts are needed)   
    + [Fasta files](#fasta-files)  

* [Steps to create simulated CYP2D6 sv samples](#steps-to-create-simulated-cyp2d6-sv-samples)

## Samples description  

### Variants  
1. **nsv38883980_cyp2d6_del** whole CYP2D6 deletion (breakpoints as in `nsv38883980` `chr22:42126499-42130881`)  
2. **wide_pink_cyp2d6_del** deletion of fragment between CYP2D6 and CYP2D7 (breakpoints in 8th intron of CYP2D6 and CYP2D7 "wide pink regions", see *X.Chen et al. 2021 Fig1*) `chr22:42126753-42140457`  
3. **narrow_pink_cyp2d6_del** deletion of fragment between CYP2D6 and CYP2D7 (breakpoints in 1st intron of CYP2D6 and CYP2D7 "narrow pink regions", see *X.Chen et al. 2021 Fig1*)  `chr22:42130810-42144483`  
4. **between_rep_cyp2d6_del** deletion between REP6 and REP7 (removes whole CYP2D6 gene, see *X.Chen et al. 2021 Fig1*) `chr22:42123846-42135998`  
5. **between_rep_cyp2d6_dup** duplication of the region between REP6 and REP7 (duplicates whole CYP2D6 gene, see *X.Chen et al. 2021 Fig1*) `chr22:42123846-42135998`  
6. **normal** no variant  
![X. Chen et al. 2021 Fig1](./Fig1.png) 
  
### Reference reads  
WGS 30x reads that align to:  
* pharmacogenes (CYP26A1, CYP2A13, CYP2B6, CYP2C19, CYP2C8, CYP2C9, CYP2F1, CYP2J2, CYP2R1, CYP2S1, CYP2W1, CYP3A4, CYP3A43, CYP3A5, CYP3A7, CYP4F2, NUDT15, SLCO1B1 plus +/- 1kg)   `wgs-1kb-30x-pharmacogenes`

or to 
* Cyrius normalization regions (without padding) `wgs-1kb-30x-cyrius-panel`, downloaded from [here](https://raw.githubusercontent.com/Illumina/Cyrius/master/data/CYP2D6_region_38.bed), only regions with `norm` in 5th column were used.

 
  

### Haploid reads     
WGS 15x reads than align to:  

* `wgs-100kb-15x-cyp2d6` 100kb region around the CYP2D6 gene (`chr22:42026499-42230865`)   
* `wgs-1kb-15x-cyp2d6` 1kb region around the CYP2D6-CYP2D7 genes (`chr22:42125499-42150455`)  

### Diploid reads  
WGS 30x reads created as sum of reference and CYP2D6 reads:  
* `ready-diploid-wgs-100kb-30x-cyp2d6`: normal CYP2D6 allele (15x) + variant CYP2D6 allele (15x) with 100kb padding - for smoove
* `ready-diploid-wgs-1kb-30x-cyrius-panel`: normal CYP2D6 allele (15x) + variant CYP2D6 allele (15k) with 1kb padding + cyrius reference reads (30x)  - for Cyrius. For these reads bam files and Cyrius outputs are also included.
* `ready-diploid-wgs-1kb-30x-pharmacogenes`: normal CYP2D6 allele (15x) + variant CYP2D6 allele (15k) with 1kb padding + pharmacogenes reference reads (30x) - for ExomeDepth  

### Fasta files  
It is easier to simulate reads for long structural variants with reference containing them than to prepare the vcf file (also variant longer than 8kb are not read from the VCF). Therefore, I prepared reference fasta files with the above variants and modified the simulate-neat.wdl to use them as input. 
* `long-ref-fasta`: 
  - whole chromosome 22 extracted from the grch38-no-alt fasta with the CYP2D6 variants inserted    
* `100kb-fasta`: 
  - fasta files with the `chr22:42026499-42230865` region with the CYP2D6 variants inserted
  -  bed files with regions boundaries
* `1kb-fasta`: 
  - fasta files with the `chr22:42125499-42150455` region with the CYP2D6 variants inserted bed files with regions boundaries
* `pharmacogenes-fasta`: 
  - fasta file with pharmacogenes (+/-1kb) 
  - bed file with regions boundaries
* `cyrius-panel-fasta`: 
  - fasta file with Cyrius regions (defined [here](https://raw.githubusercontent.com/Illumina/Cyrius/master/data/CYP2D6_region_38.bed); only norm, without CYP2D7, CYP2D6 and spacer)
  -  bed and tsv files with regions boundaries  
The directories contain also fai files. The bed files may be used to restrict simulations with `long-fasta` files as input. Note, that regions boundaries for sequences with SV differ from boundaries for normal fasta. 
   

## Steps to create simulated CYP2D6 sv samples  

1. Create the whole chr22 fasta files (normal and with structural variants):    
```bash

## get scripts and tsv file with regions
wget https://gitlab.com/intelliseq/pgx/-/raw/sv-version@0.0.1/resources/simulated-sv-samples/del_script.sh
wget https://gitlab.com/intelliseq/pgx/-/raw/sv-version@0.0.1/resources/simulated-sv-samples/cyp2d6_del.tsv 
wget  https://gitlab.com/intelliseq/pgx/-/raw/sv-version@0.0.1/resources/simulated-sv-samples/dup_script.sh  

##run scripts
chmod +x del_script.sh
./del_script.sh
chmod +x dup_script.sh
./dup_script.sh

## clean up
rm del_script.sh dup_script.sh cyp2d6_del.tsv

## create normal fasta
samtools faidx /data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa chr22 \
  -o /data/public/intelliseqngs/pgx/resources/simulated-sv-samples/fasta-files/long-ref-fasta/normal.fa
samtools faidx /data/public/intelliseqngs/pgx/resources/simulated-sv-samples/fasta-files/long-ref-fasta/normal.fa
``` 
  
2. Create smaller regions fasta files (normal and with SV):  
```bash
## get files
cd /data/public/intelliseqngs/pgx/resources/simulated-sv-samples/fasta-files
wget https://gitlab.com/intelliseq/pgx/-/raw/sv-version@0.0.1/resources/simulated-sv-samples/regions-only-fasta.sh
wget https://gitlab.com/intelliseq/pgx/-/raw/sv-version@0.0.1/resources/simulated-sv-samples/cyp2d6-regions.tsv
chmod +x regions-only-fasta.sh

## prepare 100kb fasta and bed files  
mkdir 100kb-fasta
./regions-only-fasta.sh 1
mv *.fa 1000kb-fasta
mv *.fa.fai 1000kb-fasta
mv *.bed 1000kb-fasta

## prepare 1kb fasta and bed files
mkdir 1kb-fasta 
./regions-only-fasta.sh 2
mv *.fa 1kb-fasta
mv *.fa.fai 1b-fasta
mv *.bed 1kb-fasta

## clean 
rm regions-only-fasta.sh cyp2d6-regions.tsv
```
  
3. Create reference (pharmacogenes and Cyrius reference region fasta files):   
```bash
## get script
wget https://gitlab.com/intelliseq/pgx/-/raw/sv-version@0.0.1/resources/simulated-sv-samples/biomart.sh 
chmod +x biomart.sh

## run script
./biomart.sh
```

4. Simulate reads

Simulate reads with the slightly modified version of the [simulate-neat.wdl](https://gitlab.com/intelliseq/pgx/-/raw/sv-version@0.0.1/resources/simulated-sv-samples/simulate-neat.wdl) task with fasta files as input.   
Json files:  
 * for simulation of haploid reads around the CYP2D6 (`wgs-100kb-15x-cyp2d6` and `wgs-1kb-15x-cyp2d6`): [sv-variant-template.json](https://gitlab.com/intelliseq/pgx/-/raw/sv-version@0.0.1/resources/simulated-sv-samples/sv-variant-template.json)   
 * for simulation of pharmacogenes reference `wgs-1kb-30x-pharmacogenes`: [pharmacogenes-ref-template.json](https://gitlab.com/intelliseq/pgx/-/raw/sv-version@0.0.1/resources/simulated-sv-samples/pharmacogenes-ref-template.json)  
 * for simulation of cyrius reference `wgs-1kb-30x-cyrius-panel`: [pharmacogenes-ref-template.json](https://gitlab.com/intelliseq/pgx/-/raw/sv-version@0.0.1/resources/simulated-sv-samples/cyrius-ref-template.json)  

 The following seed values were used:

> ##simulation 'rng_seed_value':  
> between_rep_cyp2d6_del =>  1  
> between_rep_cyp2d6_dup => 2  
> narrow_pink_cyp2d6_del => 5  
> normal => 0  
> nsv38883980_cyp2d6_del =>  6  
> wide_pink_cyp2d6_del =>  3  
> cyrius-panel =>  1  
> pharmacogenes => 0   

 5. Prepare diploid reads by joining haploid ans reference reads (`ready-diploid-wgs-100kb-30x-cyp2d6`, `ready-diploid-wgs-1kb-30x-cyrius-panel`, `ready-diploid-wgs-1kb-30x-pharmacogenes`) 
 ```bash
wget https://gitlab.com/intelliseq/pgx/-/raw/sv-version@0.0.1/resources/simulated-sv-samples/make-diploids.sh 
chmod +x make-diploids.sh
./make-diploids
  ```

6. Cyrius validation  
Reads from the `/data/public/intelliseqngs/pgx/resources/simulated-sv-samples/diploid-reads/ready-diploid-wgs-1kb-30x-cyrius-panel` directory were align to the grch38-no-alt reference with [`fq-bwa-align.wdl v1.6.0`](https://gitlab.com/intelliseq/workflows/-/raw/dev/src/main/wdl/modules/fq-bwa-align/fq-bwa-align.wdl) and variants were called with [`pgx-cyrius.wdl v1.1.0`](https://gitlab.com/intelliseq/workflows/-/raw/dev/src/main/wdl/tasks/pgx-cyrius/pgx-cyrius.wdl). Results:

> **between_rep_cyp2d6_del**   
> genotype: `*1/*5`  
> filter: PASS  

  
> **between_rep_cyp2d6_dup**   
> genotype: `*1/*1x2`   
>  filter: PASS  
  
    

> **narrow_pink_cyp2d6_del**  (star allele not assigned, not sure if deletion of first exon detected(?))  
> genotype: null   
> filter: null    
> CNV_group: `star13intron1`    <= **hybrid gene, intron1 -`ok`**  
> Raw_star_allele: ["*1_*1"]  
> Exon9_CN: 2  
> CNV_consensus: "2,2,2,2,1"
 
> **wide_pink_cyp2d6_del**  
> genotype: `*1/*13`  
> filter: PASS   
> CNV_group: `star13`  
> Raw_star_allele: ["*1"]  
> Exon9_CN: null  
> CNV_consensus": "2,2,1,1,1"  

> **nsv38883980_cyp2d6_del**  
> genotype: `*1/*5`  
> filter: PASS  
 




  












