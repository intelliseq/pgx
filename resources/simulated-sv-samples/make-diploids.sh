#!/bin/bash

DIR="/data/public/intelliseqngs/pgx/resources/simulated-sv-samples/"
for i in $(ls "$DIR"/haploid-reads/wgs-1kb-15x-cyp2d6/*_1.fq.gz | grep -v 'normal');do
  name=$(basename $i | sed 's/_1.fq.gz//')
  cat "$DIR"/haploid-reads/wgs-1kb-15x-cyp2d6/normal_1.fq.gz \
      "$i" \
      "$DIR"/reference-reads/wgs-1kb-30x-pharmacogenes/pharmacogenes-panel_1.fq.gz \
    > "$DIR"/diploid-reads/ready-diploid-wgs-1kb-30x-pharmacogenes/"$name"_1.fq.gz

  cat "$DIR"/haploid-reads/wgs-1kb-15x-cyp2d6/normal_2.fq.gz \
      "$DIR"/haploid-reads/wgs-1kb-15x-cyp2d6/"$name"_2.fq.gz \
      "$DIR"/reference-reads/wgs-1kb-30x-pharmacogenes/pharmacogenes-panel_2.fq.gz \
    > "$DIR"/diploid-reads/ready-diploid-wgs-1kb-30x-pharmacogenes/"$name"_2.fq.gz
done

for i in $(ls "$DIR"/haploid-reads/wgs-100kb-15x-cyp2d6/*_1.fq.gz | grep -v 'normal');do
  name=$(basename $i | sed 's/_1.fq.gz//')
  cat "$DIR"/haploid-reads/wgs-100kb-15x-cyp2d6/normal_1.fq.gz \
       "$i" \
    > "$DIR"/diploid-reads/ready-diploid-wgs-100kb-30x-cyp2d6/"$name"_1.fq.gz

  cat "$DIR"/haploid-reads/wgs-100kb-15x-cyp2d6/normal_2.fq.gz \
      "$DIR"/haploid-reads/wgs-100kb-15x-cyp2d6/"$name"_2.fq.gz \
     > "$DIR"/diploid-reads/ready-diploid-wgs-100kb-30x-cyp2d6/"$name"_2.fq.gz
done

for i in $(ls "$DIR"/haploid-reads/wgs-1kb-15x-cyp2d6/*_1.fq.gz | grep -v 'normal');do
  name=$(basename $i | sed 's/_1.fq.gz//')
  cat "$DIR"/haploid-reads/wgs-1kb-15x-cyp2d6/normal_1.fq.gz \
      "$i" \
      "$DIR"/reference-reads/wgs-1kb-30x-cyrius-panel/cyrius-panel_1.fq.gz \
     > "$DIR"/diploid-reads/ready-diploid-wgs-1kb-30x-cyrius-panel/"$name"_1.fq.gz

  cat "$DIR"/haploid-reads/wgs-1kb-15x-cyp2d6/normal_2.fq.gz \
      "$DIR"/haploid-reads/wgs-1kb-15x-cyp2d6/"$name"_2.fq.gz \
      "$DIR"/reference-reads/wgs-1kb-30x-cyrius-panel/cyrius-panel_2.fq.gz \
    > "$DIR"/diploid-reads/ready-diploid-wgs-1kb-30x-cyrius-panel/"$name"_2.fq.gz
done
