## pgx-polygenic-to-genotype-by.vcf.gz
### Current version: 

**Date**: 13.05.2022

### Instructions:

0. Be in the directory from [readme](../bref3-reference/readme.md).
1. Run this script:
```bash
mkdir tmp
for GENE in CYP26A1 CYP2B6 CYP2C8 CYP2D6 CYP2J2 CYP2S1 CYP3A4 CYP3A5 CYP4F2 SLCO1B1 CYP2A13 CYP2C19 CYP2C9 CYP2F1 CYP2R1 CYP2W1 CYP3A43 CYP3A7 NUDT15; do
 grep ^## out/$GENE/$GENE-merged.vcf > tmp/$GENE.vcf
 echo -e "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\tSAMPLE" >> tmp/$GENE.vcf
 grep -v ^# out/$GENE/$GENE-merged.vcf | cut -f1-9 | sed 's/$/\t0\/0/g'>> tmp/$GENE.vcf
 bgzip tmp/$GENE.vcf
 tabix -p vcf tmp/$GENE.vcf.gz
done
ls tmp/* | grep -v tbi > to-merge.list
bcftools concat -a -f to-merge.list| bcftools sort -o pgx-polygenic-to-genotype-by.vcf
rm -r tmp
bgzip pgx-polygenic-to-genotype-by.vcf
tabix -p vcf pgx-polygenic-to-genotype-by.vcf.gz
```
2. Update `pgx-polygenic-to-genotype-by.vcf.gz` and `pgx-polygenic-to-genotype-by.vcf.gz.tbi` in dockerfile [`resources-pgx-polygenic`](../../src/main/docker/resources-pgx-polygenic/Dockerfile) and here.
3. Update docker image in task [`resources-pgx-polygenic`](../../src/main/wdl/tasks/resources-pgx-polygenic/resources-pgx-polygenic.wdl) and run tests.
4. Change date in _current version_ above


## pgx-polygenic-with-snps-to-genotype-by.vcf.gz
### Current version: 

**Date**: 06.10.2022

### Instructions:

0. Be in this directory on anakin.
1. Run:
 ```
 zcat /data/public/intelliseqngs/pgx/resources/pgx-only-snp/pgx-snp.vcf.gz | sed 's/\.\/\./0\/0/g' > pgx-snp.vcf
 bgzip pgx-snp.vcf
 tabix -p vcf pgx-snp.vcf.gz
 bcftools concat -a pgx-polygenic-to-genotype-by.vcf.gz pgx-snp.vcf.gz | bcftools sort \
 | bcftools norm -d none -c s --fasta-ref /data/public/intelliseqngs/workflows/resources/reference-genomes/grch38-no-alt-analysis-set/GRCh38.no_alt_analysis_set.fa \
 | sed 's/2\/2/0\/0/g' > pgx-polygenic-with-snps-to-genotype-by.vcf
 ```
2. Manually change line `chr19    38499646    rs121918596    GAG    -    .    PASS    GENE=RYR1    GT    0/0` to ` chr19    38499644    rs121918596    TGGA    T    .    PASS    GENE=RYR1    GT    0/0`
3. Update `pgx-polygenic-with-snps-to-genotype-by.vcf.gz` and `pgx-polygenic-with-snps-to-genotype-by.vcf.gz.tbi` in dockerfile [`resources-pgx-polygenic`](../../src/main/docker/resources-pgx-polygenic/Dockerfile) and here.
4. Update docker image in task [`resources-pgx-polygenic`](../../src/main/wdl/tasks/resources-pgx-polygenic/resources-pgx-polygenic.wdl) and run tests.
5. Change date in _current version_ above
