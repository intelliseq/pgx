### Current version: 

**Date**: 13.05.2022

**PharmVar:** 5.1.14 - last updated 02.05.2022

**Beagle bref3:** 05May22.33a (beagle 5.4)

### Instructions:

0. Create a separate directory and move into it.
1. Download database from https://www.pharmvar.org/download (click Download Complete Database) and unzip.
2. Remove folders `GRCh37` and `RefSeqGene` and file `*.haplotypes.fasta` for each gene. Remove `GRCh38/*.haplotypes.tsv` and directory `CYP2D6/M33388`. Also gene `DPYD` should be removed - it does not have star alleles, only rs. 
```
PHARMVAR_VERSION=5.1.14
unzip pharmvar-$PHARMVAR_VERSION.zip
cd pharmvar-$PHARMVAR_VERSION
rm -r */GRCh37 */RefSeqGene CYP2D6/M33388
rm */*.haplotypes.fasta */GRCh38/*.haplotypes.tsv
rm -r DPYD
cd ..
```
3. Download `bref3.$VERSION.jar` from https://faculty.washington.edu/browning/beagle/beagle.html#download.
4. Copy `create-bref3-reference.sh` to your directory and check/update pharmvar and beagle version (`PHARMVAR_VERSION` and `BEAGLE_VERSION` variables).
5. Run `create-bref3-reference.sh`.
6. Create new vcf to use in `gvcf-genotype-by-vcf.wdl` task, using [readme](../vcf-to-genotype-by/readme.md).
7. Copy `bref3` content to `/data/public/intelliseqngs/pgx/resources/bref3-reference/bref3-$DATE` and remove your directory.
8. Update current version section in this readme.


#### Comments
When updating remove lines for fixing CYP2D6*149 duplicated line (chr22 42127608) - Version 5.1.15 has it fixed.
