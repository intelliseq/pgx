#!/bin/bash

# Define default values for command-line arguments
interval="all"
outname="out"

# Parse command-line arguments
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    --phased)
    phased="$2"
    shift
    shift
    ;;
    --raw)
    raw="$2"
    shift
    shift
    ;;
    --interval)
    interval="$2"
    shift
    shift
    ;;
    --out-name)
    outname="$2"
    shift
    shift
    ;;
    *)
    echo "Unknown argument: $key"
    exit 1
    ;;
esac
done

# Check if the required arguments are provided
if [[ -z "$phased" || -z "$raw" ]]
then
    echo "Usage: bash fix-beagle-phasing.sh --phased phased.vcf.gz --raw sorted.vcf.gz [--interval \"chr22:10000-20000\"] [--out-name fixed]"
    exit 1
fi

# Determine the interval to extract
if [[ "$interval" == "all" ]]
then
    interval_str=""
else
    interval_str="-r $interval"
fi

# Filter the VCF file for the specified interval
bcftools view -Ov $raw $interval_str > filtered.vcf

# Write header to output file
zcat $phased | grep ^## > $outname.vcf
zcat $raw | grep ^##INFO >> $outname.vcf
zcat $raw | grep ^##FORMAT >> $outname.vcf
zcat $phased | grep ^#CHROM >> $outname.vcf

# Run check by line in filtered file
while read line; do

 # Header
 if [[ $line != \#* ]]; then
  # Get line from phased
  chr=$(echo $line | awk '{print $1}')
  pos=$(echo $line | awk '{print $2}')
  line_phased=$(zcat $phased | sed -n "/^${chr}\t${pos}\t/p")

  # Get genotypes (in forms 00,01,11 only)
  genotype_phased=$(echo $line_phased | awk '{print substr($10,1,1) substr($10,3,1)}' | sed 's/10/01/g')
  genotype_raw=$(echo $line | awk '{print substr($10,1,1) substr($10,3,1)}')

  # Compare genotypes - if they are the same, use line from phased.vcf
  if [[ $genotype_phased == $genotype_raw ]]; then
   echo -e "$line_phased" >> $outname.vcf
  else
   echo -e "$line" >> $outname.vcf
  fi
 fi

done < filtered.vcf

rm filtered.vcf


# Bgzip and index the output file
bgzip -f $outname.vcf
tabix -f -p vcf $outname.vcf.gz
