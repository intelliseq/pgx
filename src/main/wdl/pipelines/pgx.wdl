import "https://gitlab.com/intelliseq/workflows/raw/resources-kit@1.3.0/src/main/wdl/tasks/resources-kit/resources-kit.wdl" as resources_kit_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-organize@2.2.3/src/main/wdl/tasks/fq-organize/fq-organize.wdl" as fq_organize_task
import "https://gitlab.com/intelliseq/workflows/raw/fq-bwa-align@1.6.0/src/main/wdl/modules/fq-bwa-align/fq-bwa-align.wdl" as fq_bwa_align_module
import "https://gitlab.com/intelliseq/workflows/raw/bam-varcalling@1.3.7/src/main/wdl/modules/bam-varcalling/bam-varcalling.wdl" as bam_varcalling_module
import "https://gitlab.com/intelliseq/workflows/raw/gvcf-genotype-by-vcf@2.0.2/src/main/wdl/tasks/gvcf-genotype-by-vcf/gvcf-genotype-by-vcf.wdl" as gvcf_genotype_by_vcf_task
import "https://gitlab.com/intelliseq/workflows/raw/pgx-genotyping-report@2.2.1/src/main/wdl/modules/pgx-genotyping-report/pgx-genotyping-report.wdl" as pgx_genotyping_report_module
import "https://gitlab.com/intelliseq/pgx/raw/pgx-polygenic@1.1.0/src/main/wdl/modules/pgx-polygenic/pgx-polygenic.wdl" as pgx_polygenic_module
import "https://gitlab.com/intelliseq/workflows/raw/vcf-var-filter@1.0.5/src/main/wdl/modules/vcf-var-filter/vcf-var-filter.wdl" as vcf_var_filter_module
import "https://gitlab.com/intelliseq/workflows/raw/sv-calling@1.8.0/src/main/wdl/modules/sv-calling/sv-calling.wdl" as sv_calling_module
import "https://gitlab.com/intelliseq/workflows/raw/bco@1.1.0/src/main/wdl/modules/bco/bco.wdl" as bco_module

workflow pgx {

  String sample_id = "no_id_provided"
  # Java options for bam_metrics and bam_gatk_hc
  String java_mem_options =  "-Xms4g -Xmx63g" # use "-Xms4g -Xmx8g" on anakin

  # Start from fastq files
  Array[File]? fastqs
  Boolean is_fastqs_defined = defined(fastqs)
  Array[File]? fastqs_left
  Boolean is_fastqs_left_defined = defined(fastqs_left)
  Array[File]? fastqs_right

  # Start from bam file (with or without gvcf)
  File? bam
  File? bai

  # Start from gvcf file (only with bam)
  File? gvcf_gz
  File? gvcf_gz_tbi

  String reference_genome = "hg38"  # or "grch38-no-alt"
  File? interval_list
  String kit = "custom_pgx"  # or "genome"

  # AD filtering
  Float ad_binom_threshold = 0.01
  Boolean apply_ad_filter = false

  # sv parameters
  Boolean run_sv_calling = true
  Boolean add_snp_data_to_sv = true
  Float del_cov_max = 0.75
  Float dup_cov_min = 1.3
  Boolean exclude_lcr = true
  Boolean exclude_chroms = true
  Float panel_threshold1 = 30.0
  Float panel_threshold2 = 45.0
  String include_ci = "yes"   ## other option "no"
  Boolean apply_rank_filter = true
  String rank_threshold = "uncertain" ## other options: "likely_benign", "uncertain", "likely_pathogenic", "pathogenic"
  Boolean create_pictures = true ## create breakpoint pictures
  Boolean create_coverage_plots = true ## create coverage plots


  String pipeline_name = "pgx"
  String pipeline_version = "2.1.0"

  # 1. Prepare interval_list if not defined
if (!defined(interval_list)) { 
    call resources_kit_task.resources_kit {
         input:
                kit = kit,
                reference_genome = reference_genome
        }
    File interval_list_resources = resources_kit.interval_list[0]
    }

  # 2a. Start analysis from fastq file
    if(is_fastqs_defined || is_fastqs_left_defined) {
        if(is_fastqs_defined) {
            # Organise fastq files
            call fq_organize_task.fq_organize {
                input:
                    fastqs = fastqs,
                    paired = true
            }
       }

    Array[File] fastqs_1 = select_first([fq_organize.fastqs_1, fastqs_left])
    Array[File] fastqs_2 = select_first([fq_organize.fastqs_2, fastqs_right])

  # Align reads
    call fq_bwa_align_module.fq_bwa_align {
                input:
                    fastqs_left = fastqs_1,
                    fastqs_right = fastqs_2,
                    sample_id = sample_id,
                    reference_genome = reference_genome
           }
    }

# 2b. Start from bam file
    File bam_to_genotype = select_first([fq_bwa_align.recalibrated_markdup_bam, bam])
    File bai_to_genotype = select_first([fq_bwa_align.recalibrated_markdup_bai, bai])


    File interval = select_first([interval_list_resources, interval_list])

# 2c. Call variants if no gvcf provided
    if(!defined(gvcf_gz) || run_sv_calling) {
        call bam_varcalling_module.bam_varcalling {
            input:
                input_bam = bam_to_genotype,
                input_bai = bai_to_genotype,
                sample_id = sample_id,
                interval_list = interval,
                reference_genome = reference_genome,
                max_no_pieces_to_scatter_an_interval_file = 6,
                run_bam_concat_task = false,
                run_gvcf_call_variants_task = true,
                haplotype_caller_java_mem = java_mem_options         }


        # 4. Filter variants
        if(run_sv_calling) {
            call vcf_var_filter_module.vcf_var_filter {
                input:
                    vcf_gz = bam_varcalling.vcf_gz,
                    vcf_gz_tbi = bam_varcalling.vcf_gz_tbi,
                    sample_id = sample_id,
                    interval_list = interval,
                    ref_genome = reference_genome,
                    apply_ad_filter = apply_ad_filter,
                    ad_binom_threshold = ad_binom_threshold,
                    analysis_type = "exome"  ## this should turn off the VQSR filtering (and run hard filters)
             }

        # 5. Call structural variants
            if (add_snp_data_to_sv) {
                File? vcf_to_sv = vcf_var_filter.filtered_vcf_gz
                File? vcf_tbi_to_sv = vcf_var_filter.filtered_vcf_gz_tbi
            }
            call sv_calling_module.sv_calling {
                input:
                    bam = bam_to_genotype,
                    bai = bai_to_genotype,
                    vcf_gz = vcf_to_sv,
                    vcf_gz_tbi = vcf_tbi_to_sv,
                    #gene_panel = panel_generate.panel,
                    #all_gene_panel = panel_generate.all_panels,
                    del_cov_max = del_cov_max,
                    dup_cov_min = dup_cov_min,
                    exclude_lcr = exclude_lcr,
                    exclude_chroms = exclude_chroms,
                    panel_threshold1 = panel_threshold1,
                    panel_threshold2 = panel_threshold2,
                    include_ci =include_ci,
                    apply_rank_filter = apply_rank_filter,
                    rank_threshold =rank_threshold,
                    create_pictures = create_pictures,
                    create_coverage_plots = create_coverage_plots,
                    sample_id = sample_id,
                    reference_genome = reference_genome
                }
        }
    }

  File gvcf_to_genotype = select_first([bam_varcalling.gvcf_gz, gvcf_gz])
  File gvcf_tbi_to_genotype = select_first([bam_varcalling.gvcf_gz_tbi, gvcf_gz_tbi])

# 3. Pharmacogenomics tools module
    call pgx_genotyping_report_module.pgx_genotyping_report {
        input:
          gvcf_gz = gvcf_to_genotype,
          gvcf_gz_tbi = gvcf_tbi_to_genotype,
          sample_id = sample_id,
          bam = bam_to_genotype,
          bai = bai_to_genotype
          }

# 4. Pharmacogenomics polygenic module
    call pgx_polygenic_module.pgx_polygenic {
        input:
          gvcf_gz = gvcf_to_genotype,
          gvcf_gz_tbi = gvcf_tbi_to_genotype,
          sample_id = sample_id
          }

# 5. Merge reports
    #call pgx_json_concat_task.pgx_json_concat { input: pgx_polygenic.report

    Array[File] bcos_module = select_all([resources_kit.bco, fq_organize.bco, fq_bwa_align.bco, bam_varcalling.bco, pgx_genotyping_report.bco,vcf_var_filter.bco, sv_calling.bco])
    Array[File] stdout_module = select_all([resources_kit.stdout_log, fq_organize.stdout_log, fq_bwa_align.stdout_log, pgx_genotyping_report.stdout_log, vcf_var_filter.stdout_log, sv_calling.stdout_log])
    Array[File] stderr_module = select_all([resources_kit.stderr_log, fq_organize.stderr_log, fq_bwa_align.stderr_log, bam_varcalling.stderr_log, pgx_genotyping_report.stderr_log, vcf_var_filter.stderr_log, sv_calling.stderr_log])

     call bco_module.bco as gather_bco_info {
      input:
        bco_array = bcos_module,
        stdout_array = stdout_module,
        stderr_array = stderr_module,
        pipeline_name = pipeline_name,
        pipeline_version = pipeline_version,
        sample_id = sample_id
     }

  output {

    File? tsv = pgx_genotyping_report.tsv
    File? json = pgx_genotyping_report.json

    File polygenic_report = pgx_polygenic.polygenic_report

    ## SV CALLING
    # vcf
    File? annotated_sv_vcf_gz = sv_calling.annotated_sv_vcf_gz
    File? annotated_sv_vcf_gz_tbi = sv_calling.annotated_sv_vcf_gz_tbi

    # reports
    File? sv_tsv = sv_calling.tsv_full_report
    File? sv_igv_pngs = sv_calling.igv_pngs
    File? sv_coverage_plots = sv_calling.coverage_plots
    File? sv_report_pdf = sv_calling.sv_report_pdf
    File? sv_report_html = sv_calling.sv_report_html

    #bco, stdout, stderr
    File bco = gather_bco_info.bco_merged
    File stdout_log = gather_bco_info.stdout_log
    File stderr_log = gather_bco_info.stderr_log

  }
}

