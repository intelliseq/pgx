import "https://gitlab.com/intelliseq/pgx/raw/resources-pgx-polygenic@1.0.0/src/main/wdl/tasks/resources-pgx-polygenic/resources-pgx-polygenic.wdl" as resources_pgx_polygenic_task
import "https://gitlab.com/intelliseq/workflows/raw/gvcf-genotype-by-vcf@2.0.6/src/main/wdl/tasks/gvcf-genotype-by-vcf/gvcf-genotype-by-vcf.wdl" as gvcf_genotype_by_vcf_task
import "https://gitlab.com/intelliseq/pgx/raw/pgx-phase-beagle@1.2.0/src/main/wdl/tasks/pgx-phase-beagle/pgx-phase-beagle.wdl" as pgx_phase_beagle_task
import "https://gitlab.com/intelliseq/pgx/raw/pgx-polygenic-haplotype@1.0.1/src/main/wdl/tasks/pgx-polygenic-haplotype/pgx-polygenic-haplotype.wdl" as pgx_polygenic_haplotype_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task


workflow pgx_polygenic {

  File gvcf_gz
  File gvcf_gz_tbi
  String sample_id = "no_id_provided"

  String module_name = "pgx_polygenic"
  String module_version = "1.1.0"


    call resources_pgx_polygenic_task.resources_pgx_polygenic {
    }

    call gvcf_genotype_by_vcf_task.gvcf_genotype_by_vcf {
        input:
          sample_id = sample_id,
          sample_gvcf_gz = gvcf_gz,
          sample_gvcf_gz_tbi = gvcf_gz_tbi,
          interval_vcf_gz = resources_pgx_polygenic.pgx_polygenic_vcf_gz,
          interval_vcf_gz_tbi = resources_pgx_polygenic.pgx_polygenic_vcf_gz_tbi
        }

    call pgx_phase_beagle_task.pgx_phase_beagle {
        input:
          genotyped_vcf_gz = gvcf_genotype_by_vcf.genotyped_vcf_gz,
          sample_id = sample_id
    }

    call pgx_polygenic_haplotype_task.pgx_polygenic_haplotype {
        input:
          vcf_sorted = pgx_phase_beagle.vcf_sorted,
          vcfs_phased = pgx_phase_beagle.vcfs_phased,
          sample_id = sample_id
    }


  # after all regular tasks merge bco
  # Merge bco, stdout, stderr files
 Array[File] bco_tasks = [resources_pgx_polygenic.bco, gvcf_genotype_by_vcf.bco, pgx_phase_beagle.bco, pgx_polygenic_haplotype.bco]
 Array[File] stdout_tasks = [resources_pgx_polygenic.stdout_log, gvcf_genotype_by_vcf.stdout_log, pgx_phase_beagle.stdout_log, pgx_polygenic_haplotype.stdout_log]
 Array[File] stderr_tasks = [resources_pgx_polygenic.stderr_log, gvcf_genotype_by_vcf.stderr_log, pgx_phase_beagle.stderr_log, pgx_polygenic_haplotype.stderr_log]


 Array[Array[File]] bco_scatters = [bco_tasks]
 Array[Array[File]] stdout_scatters = [stdout_tasks]
 Array[Array[File]] stderr_scatters = [stderr_tasks]

 Array[File] bco_array = flatten(bco_scatters)
 Array[File] stdout_array = flatten(stdout_scatters)
 Array[File] stderr_array = flatten(stderr_scatters)

 call bco_merge_task.bco_merge {
   input:
       bco_array = bco_array,
       stdout_array = stdout_array,
       stderr_array = stderr_array,
       module_name = module_name,
       module_version = module_version
 }



  output {

    File genotyped_vcf_gz = gvcf_genotype_by_vcf.genotyped_vcf_gz
    Array[File] vcf_phased_beagle = pgx_phase_beagle.vcfs_phased
    File polygenic_report = pgx_polygenic_haplotype.report

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }

}
