import "https://gitlab.com/intelliseq/workflows/raw/resources-pharmcat@1.1.0/src/main/wdl/tasks/resources-pharmcat/resources-pharmcat.wdl" as resources_pharmcat_task
import "https://gitlab.com/intelliseq/workflows/raw/gvcf-genotype-by-vcf@2.0.5/src/main/wdl/tasks/gvcf-genotype-by-vcf/gvcf-genotype-by-vcf.wdl" as gvcf_genotype_by_vcf_task
import "https://gitlab.com/intelliseq/workflows/raw/pgx-pharmcat@1.5.1/src/main/wdl/tasks/pgx-pharmcat/pgx-pharmcat.wdl" as pgx_pharmcat_task
import "https://gitlab.com/intelliseq/pgx/raw/resources-pgx-polygenic@1.0.0/src/main/wdl/tasks/resources-pgx-polygenic/resources-pgx-polygenic.wdl" as resources_pgx_polygenic_task
import "https://gitlab.com/intelliseq/pgx/raw/pgx-phase-beagle@1.0.0/src/main/wdl/tasks/pgx-phase-beagle/pgx-phase-beagle.wdl" as pgx_phase_beagle_task
import "https://gitlab.com/intelliseq/pgx/raw/pgx-polygenic-haplotype@1.0.0/src/main/wdl/tasks/pgx-polygenic-haplotype/pgx-polygenic-haplotype.wdl" as pgx_polygenic_haplotype_task
import "https://gitlab.com/intelliseq/pgx/raw/vcf-get-pgx-snp@1.0.0/src/main/wdl/tasks/vcf-get-pgx-snp/vcf-get-pgx-snp.wdl" as vcf_get_pgx_snp_task
import "https://gitlab.com/intelliseq/pgx/raw/pgx-json-concat@1.0.0/src/main/wdl/tasks/pgx-json-concat/pgx-json-concat.wdl" as pgx_json_concat_task
import "https://gitlab.com/intelliseq/workflows/raw/bco-merge@1.4.0/src/main/wdl/tasks/bco-merge/latest/bco-merge.wdl" as bco_merge_task


workflow pgx_vcf {

  File vcf_gz
  File vcf_gz_tbi

  String sample_id = "no_id_provided"


  String module_name = "pgx_vcf"
  String module_version = "latest"


  #1 first task to call
  call resources_pharmcat_task.resources_pharmcat {
    }

  call gvcf_genotype_by_vcf_task.gvcf_genotype_by_vcf as genotype_vcf_pharmcat {
    input:
      sample_id = sample_id,
      sample_gvcf_gz = vcf_gz,
      sample_gvcf_gz_tbi = vcf_gz_tbi,
      interval_vcf_gz = resources_pharmcat.pharmcat_vcf_gz,
      interval_vcf_gz_tbi = resources_pharmcat.pharmcat_vcf_gz_tbi,
      interval_bed_gz = resources_pharmcat.pharmcat_bed_gz,
      interval_bed_gz_tbi = resources_pharmcat.pharmcat_bed_gz_tbi
    }

  call pgx_pharmcat_task.pgx_pharmcat {
    input:
      sample_id = sample_id,
      sample_vcf_gz = genotype_vcf_pharmcat.genotyped_vcf_gz
      }

  call resources_pgx_polygenic_task.resources_pgx_polygenic {
    }

  call gvcf_genotype_by_vcf_task.gvcf_genotype_by_vcf as genotype_vcf_polygenic {
    input:
      sample_id = sample_id,
      sample_gvcf_gz = vcf_gz,
      sample_gvcf_gz_tbi = vcf_gz_tbi,
      interval_vcf_gz = resources_pgx_polygenic.pgx_polygenic_vcf_gz,
      interval_vcf_gz_tbi = resources_pgx_polygenic.pgx_polygenic_vcf_gz_tbi
    }

  call pgx_phase_beagle_task.pgx_phase_beagle {
    input:
      genotyped_vcf_gz = genotype_vcf_polygenic.genotyped_vcf_gz,
      sample_id = sample_id
    }

  call pgx_polygenic_haplotype_task.pgx_polygenic_haplotype {
    input:
      vcf_sorted = pgx_phase_beagle.vcf_sorted,
      vcfs_phased = pgx_phase_beagle.vcfs_phased,
      sample_id = sample_id
    }


  call vcf_get_pgx_snp_task.vcf_get_pgx_snp {
    input:
      sample_id = sample_id,
      vcf_gz = vcf_gz,
      vcf_gz_tbi = vcf_gz_tbi
    }

  call pgx_json_concat_task.pgx_json_concat {
    input:
      sample_id = sample_id,
      polygenic_json = pgx_polygenic_haplotype.report,
      snps_vcf_gz = vcf_get_pgx_snp.snps_vcf_gz
  }
  # after all regular tasks merge bco
  # Merge bco, stdout, stderr files
 Array[File] bco_tasks = [resources_pharmcat.bco, genotype_vcf_pharmcat.bco, pgx_pharmcat.bco, resources_pgx_polygenic.bco, genotype_vcf_polygenic.bco, pgx_phase_beagle.bco, pgx_polygenic_haplotype.bco]
 Array[File] stdout_tasks = [resources_pharmcat.stdout_log, genotype_vcf_pharmcat.stdout_log, pgx_pharmcat.stdout_log,resources_pgx_polygenic.stdout_log, genotype_vcf_polygenic.stdout_log, pgx_phase_beagle.stdout_log, pgx_polygenic_haplotype.stdout_log]
 Array[File] stderr_tasks = [resources_pharmcat.stderr_log, genotype_vcf_pharmcat.stderr_log, pgx_pharmcat.stderr_log, resources_pgx_polygenic.stderr_log, genotype_vcf_polygenic.stderr_log, pgx_phase_beagle.stderr_log, pgx_polygenic_haplotype.stderr_log]


 Array[Array[File]] bco_scatters = [bco_tasks]
 Array[Array[File]] stdout_scatters = [stdout_tasks]
 Array[Array[File]] stderr_scatters = [stderr_tasks]

 Array[File] bco_array = flatten(bco_scatters)
 Array[File] stdout_array = flatten(stdout_scatters)
 Array[File] stderr_array = flatten(stderr_scatters)

 call bco_merge_task.bco_merge {
   input:
       bco_array = bco_array,
       stdout_array = stdout_array,
       stderr_array = stderr_array,
       module_name = module_name,
       module_version = module_version
 }



  output {

    File pgx_report_html = pgx_pharmcat.pgx_report_html
    File pgx_report_json = pgx_pharmcat.pgx_report_json
    File pgx_matcher_html = pgx_pharmcat.pgx_matcher_html
    File pgx_matcher_json = pgx_pharmcat.pgx_matcher_json

    File polygenic_report = pgx_polygenic_haplotype.report
    File snp_polygenic_concat_json = pgx_json_concat.concat_json

    File stdout_log = bco_merge.stdout_log
    File stderr_log = bco_merge.stderr_log
    File bco = bco_merge.bco

  }

}
