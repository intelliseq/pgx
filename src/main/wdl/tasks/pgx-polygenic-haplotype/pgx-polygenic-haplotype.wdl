workflow pgx_polygenic_haplotype_workflow
{
  call pgx_polygenic_haplotype
}

task pgx_polygenic_haplotype {

  File vcf_sorted
  Array[File] vcfs_phased
  String sample_id = "no_id_provided"

  String task_name = "pgx_polygenic_haplotype"
  String task_version = "1.0.1"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/pgx-polygenic-haplotype:1.0.1"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   #for unphased
   tabix -p vcf ${vcf_sorted}

   for GENE in "CYP26A1" "CYP2A13" "CYP2B6" "CYP2C19" "CYP2C8" "CYP2C9" "CYP2D6" "CYP2F1" "CYP2J2" "CYP2R1" "CYP2S1" "CYP2W1" "CYP3A4" "CYP3A43" "CYP3A5" "CYP3A7" "CYP4F2" "NUDT15" "SLCO1B1"; do
      gene=$(echo $GENE | awk '{print tolower($0)}')
      pgstk pgs-compute --vcf ${vcf_sorted} --model /resources/models/$gene.yml --print  | jq '{"unphased":.haplotype_model.haplotypes.matching_haplotypes}' > unphased_$GENE.json
   done

   #for phased
   for file in ${sep=' ' vcfs_phased}; do
      tabix -p vcf $file
      GENE=$(basename $file "-phased-fixed.vcf.gz" | sed 's/.*_//' )
      gene=$(echo $GENE | awk '{print tolower($0)}')
      pgstk pgs-compute --vcf $file --model /resources/models/$gene.yml --print  | jq '{"phased":.haplotype_model.haplotypes.matching_haplotypes}' > phased_$GENE.json
   done

   #CYP2R1 can't be phased by beagle
      echo '{"phased":[]}' >phased_CYP2R1.json

   #merge jsons
   for GENE in "CYP26A1" "CYP2A13" "CYP26A1" "CYP2A13" "CYP2B6" "CYP2C19" "CYP2C8" "CYP2C9" "CYP2D6" "CYP2F1" "CYP2J2" "CYP2R1" "CYP2S1" "CYP2W1" "CYP3A4" "CYP3A43" "CYP3A5" "CYP3A7" "CYP4F2" "NUDT15" "SLCO1B1"; do
    jq -s 'add' phased_$GENE.json unphased_$GENE.json | jq --arg gene "$GENE" '{$gene, "haplotypes":.}' > $GENE-merged.json
   done

   rm phased_*.json unphased_*.json
   jq -s '{"polygenic":.}' *-merged.json > ${sample_id}-polygenic-haplotype.json
   rm *-merged.json


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File report = "${sample_id}-polygenic-haplotype.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
