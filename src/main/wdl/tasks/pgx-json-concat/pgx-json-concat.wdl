workflow pgx_json_concat_workflow
{
  call pgx_json_concat
}

task pgx_json_concat {

  File snps_vcf_gz
  File polygenic_json
  String sample_id = "no_id_provided"

  String task_name = "pgx_json_concat"
  String task_version = "1.0.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/ubuntu-minimal-20.04:3.0.5"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    echo "{" > ${sample_id}-snp_polygenic_concat.json
    jq -s '.[]."polygenic"|.[]."haplotypes"."phased"|.[0]' ${polygenic_json} | grep -v null \
      | sed 's/\"//g' | awk -F'[*_]' '{print "\""$1"\":\"*"$2"/*"$4"\","}' \
      >> ${sample_id}-snp_polygenic_concat.json
    grep -F "CYP2R1*" ${polygenic_json}| head -1 | sed 's/\"//g' | sed "s/ //g" \
      | awk -F'[*_]' '{print "\""$1"\":\"*"$2"/*"$4"\","}' \
      >> ${sample_id}-snp_polygenic_concat.json

    zcat ${snps_vcf_gz} | grep -v ^# > variants.txt
    while read line; do
      GENE=$(echo $line | sed 's/.*GENE=//' | cut -d';' -f1)
      RS=$(echo $line | cut -d" " -f3)
      echo '"'$GENE'":"'$RS'",' >> ${sample_id}-snp_polygenic_concat.json
    done < variants.txt

    sed -i '$s/,/\n}/' ${sample_id}-snp_polygenic_concat.json

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File concat_json = "${sample_id}-snp_polygenic_concat.json"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
