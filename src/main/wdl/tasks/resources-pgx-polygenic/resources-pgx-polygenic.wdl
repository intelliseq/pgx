workflow resources_pgx_polygenic_workflow {

  call resources_pgx_polygenic

}

task resources_pgx_polygenic {

  Array[String]? genes_list
  Boolean telostrand_analysis = false

  String task_name = "resources_pgx_polygenic"
  String task_version = "1.2.5"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/resources-pgx-polygenic:1.2.4"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

    cp /resources/pgx-polygenic.vcf.gz pgx-polygenic.vcf.gz
    cp /resources/pgx-polygenic.bed pgx-polygenic.bed

    if ${telostrand_analysis}; then
      cp /resources/telostrand/liftover_PGD855.target.bed analysis_bed.bed
      cp /resources/telostrand/liftover_PGD855.target.bed.gz analysis_bed.bed.gz
      cp /resources/telostrand/liftover_PGD855.target.bed.gz.tbi analysis_bed.bed.gz.tbi
    fi

    if [ -n "${sep=" " genes_list}" ]; then
      mkdir selected-genes/
      # Extract the selected genes from the VCF
      echo ${sep=" " genes_list} | sed 's/ /\n/g' | \
      xargs -i -n 1 -P 24 bash -c "bcftools filter -i 'INFO/ISEQ_GENES_NAMES=\"{}\"' pgx-polygenic.vcf.gz -o selected-genes/{}-pgx-polygenic.vcf.gz -Oz"
      # Index the selected genes VCFs
      ls selected-genes/*.vcf.gz | xargs -i -n 1 -P 24  bash -c "tabix -p vcf {}"
      # Concat files
      files_to_concat=$(ls selected-genes/*.vcf.gz)
      bcftools concat $files_to_concat -a | bcftools sort -o pgx-polygenic.vcf.gz -O z

      # Extract the selected genes from the BED
      cat pgx-polygenic.bed | grep -wE "${sep="|" genes_list}" | tee pgx-polygenic.bed
    fi

    # Index the VCF
    tabix -p vcf pgx-polygenic.vcf.gz

    # VCF to interval list
    gatk VcfToIntervalList I=pgx-polygenic.vcf.gz O=pgx-polygenic.interval_list

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "32G"
    cpu: "1"
    maxRetries: 2

  }

  output {

    File pgx_polygenic_vcf_gz = "pgx-polygenic.vcf.gz"
    File pgx_polygenic_vcf_gz_tbi = "pgx-polygenic.vcf.gz.tbi"
    File pgx_polygenic_interval_list = "pgx-polygenic.interval_list"
    File pgx_polygenic_bed = "pgx-polygenic.bed"

    File? analysis_bed = "analysis_bed.bed"
    File? analysis_bed_gz = "analysis_bed.bed.gz"
    File? analysis_bed_gz_tbi = "analysis_bed.bed.gz.tbi"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
