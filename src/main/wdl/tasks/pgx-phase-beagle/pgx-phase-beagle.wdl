workflow pgx_phase_beagle_workflow
{
  call pgx_phase_beagle
}

task pgx_phase_beagle {

  File genotyped_vcf_gz
  String sample_id = "no_id_provided"

  String task_name = "pgx_phase_beagle"
  String task_version = "1.2.0"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/pgx-phase-beagle:1.1.0"

  String dollar = "$"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

   BEAGLE="/tools/beagle/5.4-05May22.33a/beagle.05May22.33a.jar"

   bcftools sort -Oz -o ${sample_id}-sorted.vcf.gz ${genotyped_vcf_gz}
   tabix -p vcf ${sample_id}-sorted.vcf.gz

   for i in "CYP26A1 chr10:93074880-93076617" "CYP2A13 chr19:41086992-41096043" "CYP2B6 chr19:40988985-41018249" "CYP2C19 chr10:94760720-94852997" "CYP2C8 chr10:95037038-95069814" "CYP2C9 chr10:94936916-94989184" "CYP2D6 chr22:42126309-42132393" "CYP2F1 chr19:41113845-41128076" "CYP2J2 chr1:59901084-59926823" "CYP2S1 chr19:41194547-41206520" "CYP2W1 chr7:983376-988813" "CYP3A4 chr7:99758059-99784474" "CYP3A43 chr7:99836453-99861634" "CYP3A5 chr7:99648290-99680606" "CYP3A7 chr7:99709061-99735408" "CYP4F2 chr19:15879620-15897579" "NUDT15 chr13:48037625-48045807" "SLCO1B1 chr12:21130884-21239391"; do
      a=( $i )

      # Run beagle
      java -jar $BEAGLE gt=${sample_id}-sorted.vcf.gz ref=/resources/bref3-reference/"${dollar}{a[0]}".bref3 out=${sample_id}_"${dollar}{a[0]}"-phased chrom="${dollar}{a[1]}"

      # Fix phasing
      bash /intelliseqtools/fix-beagle-phasing.sh \
            --phased ${sample_id}_"${dollar}{a[0]}"-phased.vcf.gz \
            --raw ${sample_id}-sorted.vcf.gz \
            --interval "${dollar}{a[1]}" \
            --out-name fixed-tmp

      bcftools norm -Oz -m -any fixed-tmp.vcf.gz > ${sample_id}_"${dollar}{a[0]}"-phased-fixed.vcf.gz

   done

   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    Array[File] vcfs_phased = glob("*phased-fixed.vcf.gz")
    File vcf_sorted = "${sample_id}-sorted.vcf.gz"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
