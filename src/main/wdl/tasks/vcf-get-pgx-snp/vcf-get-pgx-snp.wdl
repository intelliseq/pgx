workflow vcf_get_pgx_snp_workflow
{
  call vcf_get_pgx_snp
}

task vcf_get_pgx_snp {

  File vcf_gz
  File vcf_gz_tbi

  String sample_id = "no_id_provided"

  String task_name = "vcf_get_pgx_snp"
  String task_version = "latest"
  Int? index
  String task_name_with_index = if defined(index) then task_name + "_" + index else task_name
  String docker_image = "intelliseqngs/vcf-get-pgx-snp:1.0.0"

  command <<<
   set -e -o pipefail
   bash /intelliseqtools/bco-after-start.sh --task-name-with-index ${task_name_with_index}

      SAMPLE=$(bcftools query -l ${vcf_gz})
      bcftools isec -p intersect -O z -c all ${vcf_gz} /resources/pgx-snp.vcf.gz
      bcftools merge -O z intersect/0002.vcf.gz intersect/0003.vcf.gz > merged.vcf.gz
      bcftools view -O z -s $SAMPLE merged.vcf.gz > ${sample_id}-snps.vcf.gz


   bash /intelliseqtools/bco-before-finish.sh --task-name ${task_name} \
                                              --task-name-with-index ${task_name_with_index} \
                                              --task-version ${task_version} \
                                              --task-docker ${docker_image}
  >>>

  runtime {

    docker: docker_image
    memory: "1G"
    cpu: 1
    maxRetries: 2

  }

  output {

    File snps_vcf_gz = "${sample_id}-snps.vcf.gz"

    File stdout_log = stdout()
    File stderr_log = stderr()
    File bco = "bco.json"

  }

}
